//global object defined for page details containing various DOM reference
let page_info = {
    button: document.getElementById('submit'),
    button_handler: function() {
        this.button.addEventListener('click', checkInputText);
    },
    text_inputs: (function(){
            let inputs = Array.prototype.slice.call(document.querySelectorAll('input'));
            return inputs.filter(item => item.getAttribute('type') == 'text');
        })(),
    selection: document.querySelectorAll('select')
};

//in-built form validation
function checkInputText() {
    count = counter();
    if(count == page_info.text_inputs.length) {
        formValidation();
        page_info.button.addEventListener('click', formValidation);
        page_info.button.removeEventListener('click', checkInputText);
    }
}

//manual form validation
function formValidation() {
    count = counter();
    if(count != page_info.text_inputs.length) {
        checkInputText();
        page_info.button.addEventListener('click', checkInputText);
        page_info.button.removeEventListener('click', formValidation);
    }
    else {
        for(let i=0; i<page_info.selection.length; i++) {
            if(page_info.selection[i].value == '') {
                alert('Please select '
                    + page_info.selection[i].firstElementChild.textContent.toLowerCase()
                    + '!');
                break;
            }
        }
    }
}

function counter() {
    let count = 0;
    for(let i=0; i<page_info.text_inputs.length; i++) {
        if(page_info.text_inputs[i].value == '')
            break;
        count += 1;
    }
    return count;
}

window.onload = () => {
    page_info.button_handler();
};